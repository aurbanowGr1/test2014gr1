package services;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import repositories.IRepositoryCatalog;
import domain.Address;
import domain.Person;


@WebService()
public class TestService {

	@Inject
	private IRepositoryCatalog catalog;
	
	@Transactional
	public int test()
	{
		try{
			Person p = new Person();
			p.setFirstName("Jan2");
			p.setSurname("Kowalski");
			catalog.getPersons().add(p);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			return -1;
		}
		return 1;
	}
	
	public Address getAddress(int id)
	{
		return catalog.getAddresses().get(id);
	}
}
