package services.rest;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import domain.Person;
import domain.User;
import repositories.IRepositoryCatalog;
import services.dto.PersonSummaryDto;
import services.dto.ShowUsersResponseDto;
import services.dto.UserSummaryDto;


@Path("users")
public class UserRestService {


	@Inject
	IRepositoryCatalog catalog;
	

	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveUser(UserSummaryDto user)
	{
		User u = new User();
		u.setLogin(user.getLogin());
		u.setPassword(user.getPassword());
		catalog.getUsers().add(u);
		return "OK";
	}

	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public ShowUsersResponseDto getUsers()
	{
		ShowUsersResponseDto result = new ShowUsersResponseDto();
		for(User u : catalog.getUsers().getAll())
		{
			UserSummaryDto user = new UserSummaryDto();
			user.setLogin(u.getLogin());
			user.setPassword(u.getPassword());
			user.setId(u.getId());
			result.getUsers().add(user);
		}
		return result;
	}
	
}
