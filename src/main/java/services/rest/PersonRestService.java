package services.rest;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import domain.Address;
import domain.Person;
import repositories.IRepositoryCatalog;
import services.dto.AddressSummaryDto;
import services.dto.PersonDto;
import services.dto.PersonSummaryDto;

@Path("persons")
public class PersonRestService {
	
	@Inject
	IRepositoryCatalog catalog;
	
	@GET
	@Path("get")
	@Produces("application/json")
	@Transactional
	public PersonSummaryDto get()
	{
		PersonSummaryDto result = new PersonSummaryDto();
		Person p = catalog.getPersons().get(1);
		result.setId(p.getId());
		result.setFirstName(p.getFirstName());
		return result;
	}
	
	@GET
	@Path("/test")
	@Produces("text/html")
	public String test(){
		return "REST Service is running";
	}
	

	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String savePerson(PersonDto person)
	{
		Person p = new Person();
		p.setFirstName(person.getFirstName());
		p.setSurname(person.getSurname());
		for(AddressSummaryDto address: person.getAddresses())
		{
			Address a  = new Address();
			a.setCity(address.getCity());
			a.setCountry(address.getCountry());
			a.setStreet(address.getStreet());
			p.getAddresses().add(a);
			a.setPerson(p);
		}
		catalog.getPersons().add(p);
		return "OK";
	}
	
	
}
