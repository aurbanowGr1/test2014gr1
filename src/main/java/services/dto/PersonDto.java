package services.dto;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PersonDto extends PersonSummaryDto {

	private ArrayList<AddressSummaryDto> addresses;

	public PersonDto()
	{
		this.addresses=new ArrayList<AddressSummaryDto>();
	}
	
	public ArrayList<AddressSummaryDto> getAddresses() {
		return addresses;
	}

	public void setAddresses(ArrayList<AddressSummaryDto> addresses) {
		this.addresses = addresses;
	}
	
	
}
