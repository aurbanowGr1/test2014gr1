package services.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ShowUsersResponseDto {

	private List<UserSummaryDto> users = 
			new ArrayList<UserSummaryDto>();

	public List<UserSummaryDto> getUsers() {
		return users;
	}

	public void setUsers(List<UserSummaryDto> users) {
		this.users = users;
	}
	
	
}
