package services;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class EMProducer {

	@PersistenceContext(name="test2014gr1")
	@Produces
	private EntityManager em;
}
