package repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import domain.Privilege;
import repositories.IRepository;

public class JpaPrivilegeRepository implements IRepository<Privilege>{

	private EntityManager em;
	
	public JpaPrivilegeRepository(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public Privilege get(int id) {
		return em.find(Privilege.class, id);
	}

	@Override
	public List<Privilege> getAll() {
		return em.createNamedQuery("privilege.all", Privilege.class).getResultList();
	}

	@Override
	public void add(Privilege entity) {
		em.persist(entity);
		
	}

	@Override
	public void delete(Privilege entity) {

		em.remove(entity);
		
	}

	@Override
	public void update(Privilege entity) {
		// TODO Auto-generated method stub
		
	}

}
