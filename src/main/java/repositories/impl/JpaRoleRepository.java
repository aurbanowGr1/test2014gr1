package repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import domain.Role;
import repositories.IRepository;

public class JpaRoleRepository implements IRepository<Role> {

	private EntityManager em;
	
	public JpaRoleRepository(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public Role get(int id) {
		return em.find(Role.class, id);
	}

	@Override
	public List<Role> getAll() {
		return em.createNamedQuery("role.all",Role.class).getResultList();
	}

	@Override
	public void add(Role entity) {
		em.persist(entity);
		
	}

	@Override
	public void delete(Role entity) {
		em.remove(entity);
		
	}

	@Override
	public void update(Role entity) {
		// TODO Auto-generated method stub
		
	}

}
