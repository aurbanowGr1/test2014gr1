package repositories.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.soap.SOAPBinding.Use;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import domain.User;
import repositories.IRepository;

public class JpaUserRepository implements IRepository<User>{

	private EntityManager em;
	
	public JpaUserRepository(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public User get(int id) {
		return em.find(User.class, id);
	}

	@Override
	public List<User> getAll() {
		return em.createNamedQuery("user.all", User.class).getResultList();
	}

	@Override
	public void add(User entity) {
		em.persist(entity);
	}
	
	@Override
	public void delete(User entity) {

		em.remove(entity);
	}

	@Override
	public void update(User entity) {
		
	}

	
}
