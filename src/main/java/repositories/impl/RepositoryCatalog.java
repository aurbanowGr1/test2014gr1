package repositories.impl;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.metamodel.StaticMetamodel;
import javax.transaction.Transactional;

import domain.Address;
import domain.Person;
import domain.Privilege;
import domain.Role;
import domain.User;
import repositories.IRepository;
import repositories.IRepositoryCatalog;


@Stateless
public class RepositoryCatalog implements IRepositoryCatalog{

	@Inject
	private EntityManager em;
	
	@Override
	public IRepository<Person> getPersons() {
		return new JpaPersonRepository(em);
	}

	@Override
	public IRepository<User> getUsers(){
		return new JpaUserRepository(em);
	}

	@Override
	public IRepository<Address> getAddresses() {
		return new JpaAddressRepository(em);
	}

	@Override
	public IRepository<Role> getRoles() {
		return new JpaRoleRepository(em);
	}

	@Override
	public IRepository<Privilege> getPrivileges() {
		return new JpaPrivilegeRepository(em);
	}

}
