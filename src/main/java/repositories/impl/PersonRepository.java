package repositories.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;












import domain.EntityBase;
import domain.Person;
import entitybuilders.IEntityBuilder;
import repositories.IRepository;
import unitofwork.IUnitOfWork;

public class PersonRepository
	extends RepositoryBase<Person>{

	public PersonRepository(Connection connection, IEntityBuilder<Person> builder, IUnitOfWork uow) {
	
		super(connection, builder, uow);
	}

	@Override
	protected String getTableName() {
		return "person";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE person SET (firstname,surname,pesel,email)=(?,?,?,?) WHERE id=?";
	}

	@Override
	protected String getCreateQuery() {

		return "Insert into person(firstname,surname,email,pesel) values (?,?,?,?)";
		}


	@Override
	protected void prepareAddQuery(Person entity) throws SQLException {
		save.setString(1, entity.getFirstName());
		save.setString(2, entity.getSurname());
		save.setString(3, entity.getEmail());
		save.setString(4, entity.getPesel());
		
		
	}


	@Override
	protected void prepareUpdateQuery(Person entity) throws SQLException {
		update.setString(1, entity.getFirstName());
		update.setString(2, entity.getSurname());
		update.setString(3, entity.getEmail());
		update.setString(4, entity.getPesel());
		update.setLong(5, entity.getId());
		
	}
	
}
