package repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import domain.Address;
import domain.Person;
import repositories.IRepository;

public class JpaPersonRepository implements IRepository<Person>{

	private EntityManager em;
	
	public JpaPersonRepository(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public Person get(int id) {
		return em.find(Person.class, id);
	}

	@Override
	public List<Person> getAll() {
		return em.createNamedQuery("person.all", Person.class).getResultList();
	}

	@Override
	public void add(Person entity) {
		em.persist(entity);
		for(Address a: entity.getAddresses())
		{
			em.persist(a);
		}
	}

	@Override
	public void delete(Person entity) {
		em.remove(entity);
	}

	@Override
	public void update(Person entity) {
		// TODO Auto-generated method stub
		
	}

}
