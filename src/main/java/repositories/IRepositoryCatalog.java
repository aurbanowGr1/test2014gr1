package repositories;

import domain.*;

public interface IRepositoryCatalog {

	public IRepository<Person> getPersons();
	public IRepository<Address> getAddresses();
	public IRepository<User> getUsers();
	public IRepository<Role> getRoles();
	public IRepository<Privilege> getPrivileges();
}
