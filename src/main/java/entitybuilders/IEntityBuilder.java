package entitybuilders;

import java.sql.ResultSet;

import domain.EntityBase;

public interface IEntityBuilder<TEntity extends EntityBase> {

	public TEntity build(ResultSet row);
}
