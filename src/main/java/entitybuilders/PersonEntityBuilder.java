package entitybuilders;

import java.sql.ResultSet;

import domain.Person;

public class PersonEntityBuilder implements IEntityBuilder<Person>
{
	@Override
	public Person build(ResultSet rs) {
		try
		{
			Person p = new Person();
			p.setFirstName(rs.getString("firstname"));
			p.setSurname(rs.getString("surname"));
			p.setPesel(rs.getString("pesel"));
			p.setEmail("email");
			return p;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	} 

}
