package domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="privilege.all", query="SELECT p FROM Privilege p")
public class Privilege extends EntityBase  {

	public Privilege()
	{
		roles = new ArrayList<Role>();
	}
	
	private String name;
	
	@ManyToMany
	private List<Role> roles;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	
}
