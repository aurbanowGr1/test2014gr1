package domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

@Entity
@NamedQuery(name="user.all", query="SELECT u FROM User u")
public class User extends EntityBase  {

	public User()
	{
		roles = new HashSet<Role>();
	}
	

	@Size(min = 4, max = 20)
	private String login;
	

	@Size(min = 4, max = 20)
	private String password;
	
	@OneToOne(mappedBy="user")
	private Person person;
	
	@ManyToMany(mappedBy="users", cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
	private Set<Role> roles;
	
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
	
	
}
