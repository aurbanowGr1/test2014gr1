package domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="role.all", query="SELECT r FROM Role r")
public class Role  extends EntityBase {

	public Role()
	{
		users = new ArrayList<User>();
		privileges=new ArrayList<Privilege>();
	}
	
	private String name;
	
	@ManyToMany
	private List<User> users;
	
	@ManyToMany(mappedBy="roles", fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
	private List<Privilege> privileges;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public List<Privilege> getPrivileges() {
		return privileges;
	}
	public void setPrivileges(List<Privilege> privileges) {
		this.privileges = privileges;
	}
	
	
	
}
